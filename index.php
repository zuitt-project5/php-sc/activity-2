<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S2: Activity 2</title>
</head>
<body>

	<h2>Divisibles By Five</h2>
	<?php divFive(); ?>


	<?php $students = []; ?>

	<h2>Array Manipulation</h2>
	<?php array_push($students, 'Elito Penaranda'); ?>
	<pre><?php print_r($students); ?></pre>
	<pre><?php echo count($students); ?></pre>

	<?php array_push($students, 'Melane Viagedor'); ?>
	<pre><?php print_r($students); ?></pre>
	<pre><?php echo count($students); ?></pre>

	<?php array_shift($students); ?>
	<pre><?php print_r($students); ?></pre>
	<pre><?php echo count($students); ?></pre>

</body>
</html>